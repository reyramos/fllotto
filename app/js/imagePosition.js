angular.module ( 'app' ).directive
	( 'getImageSize',
	  [
		  '$window', '$timeout',
		  function( $window, $timeout ) {
			  return {
				  link:function( scope, element, attr ) {
					  $timeout(function(){
						  element.ready(__initialiaze)
					  },5000)

					function __initialiaze(){
						var offset = Number(element[0].offsetHeight) - Number(element.parent().parent().parent().parent()[0].offsetHeight);
						if(offset < 0){
							var padding = Math.abs(offset / 2);
							element.css({'margin-top':padding+'px'})
						}
					}
				  }
			  };
		  }
	  ] );