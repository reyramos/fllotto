'use strict';
/**
 * ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application, no application logic done at the upper level
 *
 */
angular.module('app').controller(
	'ApplicationController',
	[
		  '$scope','$interval','$timeout','$window','$http','$resource'
		, function
			(
				$scope,$interval,$timeout,$window,$http,$resource
				) {
		var interval,randomSpeed = 3,randoms = [];


		$scope.tickets = tickets
		$scope.jackpots = JSON.parse(localStorage.getItem("jackpots")) ||  {lotto:'&nbsp;',megamillions:'&nbsp;'};

		$scope.panels = ['front-pane','back-pane','top-pane','bottom-pane','left-pane','right-pane']
		$scope.flipper = ['front','back']
		$scope.rows = [
			[4,5,6,7,8,9,10,11],[12,13,14,15,16,17,18,19],[2,3,20,21,22,23]
		]

		getTOOLS_JSONP();
		function getTOOLS_JSONP(){
			$http({method: 'GET', url: 'http://tools.mmdn.net/fl_lotto/'}).
				success(function(data, status) {
							if(status == 200){
								$scope.jackpots = {
									lotto:data.lotto.nextjp,
									megamillions:data.megamillions.nextjp
								}

								if(typeof(Storage) !== "undefined") {
									localStorage.setItem("jackpots", JSON.stringify($scope.jackpots));
								}
							}
						}).
				error(function(data, status) {
						//keep trying to get data
					  getTOOLS_JSONP();
				});
		}

		$timeout(function(){
			//check for result every 30 minutes
			getTOOLS_JSONP();
		},(1000 * 60 * 30))


		function empty(mixed_var) {
			var undef, key, i, len;
			var emptyValues = [undef, null, false, 0, '', '0'];

			for (i = 0, len = emptyValues.length; i < len; i++) {
				if (mixed_var === emptyValues[i]) {
					return true;
				}
			}

			if (typeof mixed_var === 'object') {
				for (key in mixed_var) {
					// TODO: should we check for own properties only?
					//if (mixed_var.hasOwnProperty(key)) {
					return false;
					//}
				}
				return true;
			}

			return false;
		}



		$scope.spinners = []
		$('body').find('[data-spinner]').each(function(key){
			var data = {
				obj:this,
				active:false,
				backward: false
			}
			$scope.spinners.push(data)
		})

		//make the first spinner active
		$scope.spinners[0].active = true
		var stop = $interval(function(){
			for(var i in $scope.spinners){
				$scope.spinners[i].active = !$scope.spinners[i].active;
				$scope.spinners[i].backward = (  random(1, 20) & 1 ) ? false : true;
			}
		},5000)


		angular.element($window).on('$destroy', function() {
			$interval.cancel(stop);
			$interval.cancel(interval);
			$timeout.cancel(timeout);
		});

		var timeout = $timeout(function(){
			_contruct();
		},2000)


		function _contruct(){
			for(var i in $scope.tickets){
				$scope.tickets[i].animate = false;
				$scope.tickets[i].vertical = false;
			}

			interval = 	$interval(function(){
				for(var i =0;i<randomSpeed;i++){
					setFlippingBox(i);
				}
			},1000)
		}


		function setFlippingBox(speed){
			speed = 500 + (speed * 100)
			var rand = Number(random(1,23));
			if(rand in randoms){
				$scope.tickets[rand].animate = false;
				randoms.splice(rand, 1);
			}else{
				randoms[rand] = true;
				$scope.tickets[rand].vertical =  (  random(1, 20) & 1 ) ? false : true;
				$timeout(function(){
					$scope.tickets[rand].animate = !$scope.tickets[rand].animate;
				},speed)
			}
		}

		function random(min, man){
			return Math.floor(Math.random() * man | 0) + min
		}

	}]
)
