'use strict';
/**
 * # Main application bootstrap file
 *
 * Allows main application to be bootloaded. This seperate file is required in
 * order to properly isolate angular logic from requirejs module loading
 */
angular.bootstrap(document, ['app'])
