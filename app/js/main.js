'use strict';
// Require JS  Config File
require({
		baseUrl: '/js/',
		paths: {
			  'angular': '../lib/angular/index'
			, 'angular-resource': '../lib/angular-resource/index'
			, 'angular-sanitize': '../lib/angular-sanitize/index'
			, 'jquery': '../lib/jquery/dist/jquery'
		},
		map: {
			'*': { 'jquery': 'jquery' }, 'noconflict': { "jquery": "jquery" }
		},
		shim: {
			'app': { 'deps': [
				'angular'
				, 'angular-resource'
				, 'angular-sanitize'

			]}
			, 'angular-resource': { 'deps': ['angular'] }
			, 'angular-sanitize': { 'deps': ['angular'] }
			, 'jquery': {
				init: function ($) {
					return $.noConflict(true);
				},
				exports: 'jquery'
			}
			, 'ApplicationController_v140819': {
				'deps': [
					'app','jquery'
			]}
			, 'imagePosition': {
				'deps': [
					'app','jquery'
				]}

		}}
	, [
		'require'
		, 'ApplicationController_v140819'
		, 'imagePosition'
	]
	, function (require) {
		return require(
			[
				'bootstrap'
			]
		)
	}
);