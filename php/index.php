<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 8/5/14
 * Time: 4:10 PM
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header('content-type: application/json; charset=utf-8');

error_reporting(E_ALL);
ini_set("display_errors", 1);

$response_xml_data = file_get_contents('http://www.flalottery.com/video/en/theWinningNumber.xml');

$xml = new SimpleXMLElement($response_xml_data);
//$result = $xml->xpath('/rss/channel/item[@game="lotto"]');
$result = $xml->xpath('/rss/channel/item');


$items = array();

foreach($result as $a => $b){
	$item = array();
	foreach($b->attributes() as $a => $b) {
		$item = array_merge($item, array((string)$a => (string)$b));
	}
	$items = array_merge($items,array($item['game'] => $item));
}

echo json_encode($items);



?>