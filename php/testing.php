<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 8/5/14
 * Time: 4:10 PM
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
header('content-type: application/json; charset=utf-8');


$now = date('Gi');

$JSONSTRING = '
{"powerball":{"game":"powerball","nextjp":"$'.$now.' Million","nextdd":"Wednesday, August 20, 2014","winnum":"7-8-17-48-59 PB9 X2","windd":"Saturday, August 16, 2014","myflv":"widget_pb.flv","winnumNM":"7-8-17-48-59 PB9","name":"POWERBALL"},"lotto":{"game":"lotto","nextjp":"$16 Million","nextdd":"Wednesday, August 20, 2014","winnum":"6-8-14-18-29-37 X3","windd":"Saturday, August 16,2014","myflv":"widget_l6.flv","name":"FLORIDA LOTTO"},"mega":{"game":"mega","nextjp":"Game Ends","nextdd":"Tuesday, July 1, 2014","winnum":"4-16-33-43 MB1","windd":"Tuesday, July 1, 2014","myflv":"widget_mm.flv","name":"MEGA MONEY"},"fan5":{"game":"fan5","winnum":"2-5-9-12-30","windd":"Tuesday, August 19, 2014","myflv":"widget_f5.flv","name":"FANTASY 5"},"play4":{"game":"play4","winnumm":"4-6-5-4","midd":"Tuesday, August 19, 2014","winnume":"9-7-0-8","eved":"Tuesday, August 19, 2014","myflvm":"widget_p4_midd.flv","myflve":"widget_p4_eved.flv","name":"PLAY 4"},"cash3":{"game":"cash3","winnumm":"4-8-4","midd":"Tuesday, August 19, 2014","winnume":"4-9-8","eved":"Tuesday, August 19, 2014","myflvm":"widget_c3_midd.flv","myflve":"widget_c3_eved.flv","name":"CASH 3"},"megamillions":{"game":"megamillions","nextjp":"$'.$now.' Million","nextdd":"Friday, August 22, 2014","winnum":"22-39-56-67-71 MM15 X4","windd":"Tuesday, August 19, 2014","name":"MEGA MILLIONS"},"lucky":{"game":"lucky","nextjp":"$500,000","nextdd":"Friday, August 22, 2014","winnum":"19-27-31-35 LB11","windd":"Tuesday, August 19, 2014","myflv":"widget_lucky.flv","name":"LUCKY MONEY"}}';

echo $JSONSTRING;




?>